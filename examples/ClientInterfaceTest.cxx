// Simple test application to show how to send commands to the Igui via IPC.
// 
// To compile such an application you need to modify your requirements file including the following "use" statements:
// use IguiCommander
// use ers
// use Boost  *  TDAQCExternal
// 
// The compilation statements may as simple as:
// application     igui_test_client                -no_prototypes "../examples/ClientInterfaceTest.cxx"
// macro           igui_test_clientlinkopts        "-lIguiCommander -lers -lboost_thread-$(boost_libsuffix)"
// macro           igui_test_client_dependencies   "IguiCommander"
//
// Usage:
// Pass the partition name as the first argument.

#include <IguiCommander/IguiCommander.hh>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <ers/ers.h>

#include <boost/thread/condition.hpp>
#include <boost/thread/thread.hpp>

boost::mutex monitor;
boost::condition cnd;
bool shouldExit = false;

class IguiClient : public IPCObject<POA_iguiCommander::Client> {  
public:
  IguiClient() {};
  
  virtual ~IguiClient() {};
      
  void done(iguiCommander::UserAnswer ua) {
    // Remember that this method is always called in some CORBA thread
    // Use proper synchronization when needed
    ERS_LOG("Received answer: " << ua);
    
    boost::mutex::scoped_lock lk(monitor);
    shouldExit = true;
    cnd.notify_one();
  }
  
protected:
private:
};

int main(int argc, char** argv) {
  try {
    // Init the IPC system
    IPCCore::init(argc, argv);

    // Create the partition object
    const IPCPartition p(argv[1]);

    // Look for the Igui
    iguiCommander::Server_var s = p.lookup<iguiCommander::Server>(iguiCommander::servantName);

    if(s != 0) {
      // ---> Send simple messages to the user
      s->informUser("This is an INFORMATION message", iguiCommander::INFORMATION);
      s->informUser("This is a WARNING message", iguiCommander::WARNING);
      s->informUser("This is an ERROR message", iguiCommander::ERROR);

      // ---> Ask a question and get the user answer
      // Create the client that will receive the call-back (it should always be created on the heap)
      IguiClient* thisClient = new IguiClient();
      
      // Create the request object
      iguiCommander::ClientRequest request;
      request.clientId = CORBA::string_dup("Test Application");
      request.msg = CORBA::string_dup("You are asked to answer yes or no");
      request.clientRef = thisClient->_this();

      // Send the request to the server
      s->askUser(request);

      // Now wait for the call-back
      boost::mutex::scoped_lock lk(monitor);
      while(shouldExit == false) {
	cnd.wait(lk);
      }

      // Do not call delete on the IguiClient! Always use "_destroy()"
      thisClient->_destroy();
    } else {
      // Igui reference not found in IPC
      ERS_LOG("It looks like the Igui is not published in IPC");
    }

    ERS_LOG("Done!");
  }
  catch(ers::Issue& ex) {
    ERS_LOG("Error, got issue: " + ex.message());
  }
  catch(std::exception& ex) {
    ERS_LOG("Error, got standard exception: " + std::string(ex.what()));
  }
  catch(...) {
    ERS_LOG("Error, got an unexpected exception");
  }
}
