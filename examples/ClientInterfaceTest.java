import java.util.concurrent.atomic.AtomicBoolean;

import iguiCommander.ClientPOA;
import iguiCommander.ClientRequest;
import iguiCommander.UserAnswer;


public class ClientInterfaceTest {
    // It takes a single argument: the partition name

    public static void main(final String[] args) {
	try {
	    // Create the partition object
	    final ipc.Partition p = new ipc.Partition("part_l2ef");

	    // Look for the Igui in IPC
	    final iguiCommander.Server s = p.lookup(iguiCommander.Server.class, iguiCommander.servantName.value);

	    // ---> Send a message to the user
	    s.informUser("This is an INFORMATION message", iguiCommander.MessageSeverity.INFORMATION);
	    s.informUser("This is a WARNING message", iguiCommander.MessageSeverity.WARNING);
	    s.informUser("This is an ERROR message", iguiCommander.MessageSeverity.ERROR);

	    // ---> Ask something to the user and get back an answer
	    // Create an object implementing the client interface
	    final AtomicBoolean callbackReceived = new AtomicBoolean(false);
	    
	    final ClientPOA cl = new ClientPOA() {
		@Override
		public void done(final UserAnswer ua) {
		    // Remember that this call-back method is always executed in one of the jacorb threads
		    // Use proper synchronization if needed
		    System.out.println("The user aswer is \"" + ua.toString() + "\"");

		    synchronized(this) {
			callbackReceived.set(true);
			this.notify();
		    }
		}
	    };

	    // Create a client request
	    final ClientRequest cr = new ClientRequest("TestApplication",
						       "You can select \"yes\" or \"no\" and your answer will be reported to the client",
						       cl._this(ipc.Core.getORB()));

	    // Send the request to the Igui
	    s.askUser(cr);

	    // Wait for the answer (needed to not let the test application exit before receiving the call-back)
	    synchronized(cl) {
		try {
		    while(callbackReceived.get() == false) {
			cl.wait();
		    }
		}
		catch(final InterruptedException ex) {
		    ex.printStackTrace();
		}
	    }

	    System.out.println("Test done!");
	}
	catch(final Exception ex) {
	    // Remember that any remote call to the server may cause a CORBA exception to be thrown
	    ex.printStackTrace();
	}
    }
}
